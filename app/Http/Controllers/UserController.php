<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        return "Usuarios";
    }
    
    public function index_greet($nombre, $edad)
    {
        return "Hola, {$nombre}. Tienes {$edad} años.";
    }
}