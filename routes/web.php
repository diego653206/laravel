<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/prueba', function () {
    return "Hola mundo";
});

Route::get('/users/{Diego}/{23}', function ($nombre, $edad) {
    return "Hola, {$nombre}. Tienes {$edad} años.";
});

Route::get('/users', [UserController::class, 'index']);


Route::get('/users/{nombre}/{edad}', [UserController::class, 'index_greet']);